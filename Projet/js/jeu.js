var cartesImg = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6]; //initialisation des id sur les images
var etatsCartes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //etat initial des cartes (toutes les cartes au debut sont a 0 soit non retourne)
var cartesRetournees = []; //un array qui contiendra les id des images
var nbPairesTrouvees = 0; //nombre de paires trouvee initialise a 0
let score = 0; //initialisation du score a 0
let niveau = 0; //initialisation du niveau a 0
let card2show = document.getElementById('cards2').style.display = "none"; //deuxieme vague de carte cachee
let card3show = document.getElementById('cards3').style.display = "none"; // troisieme vague de cartes cachee
let cardshow = document.getElementById('card');
let carteshow = document.getElementById('cards2');
let carte2show = document.getElementById('cards3');


const cards = document.getElementById('card').getElementsByTagName("img");
const carte2 = document.getElementById('cards2').getElementsByTagName("img");
const carte3 = document.getElementById('cards3').getElementsByTagName("img");

/*On parcourt le tableau des objets des éléments img, chacun d'eux reçoit une fonction déclenchée par l'événement onclick.
La fonction ainsi définie est exécutée à chaque fois que l'utilisateur clique sur l'image son rôle est d'appeller controleJeu avec le numéro de l'image cliquée.*/
for (var i = 0; i < cards.length; i++) {
	cards[i].noCarte = i;
	cards[i].onclick = function () {
		controleJeu(this.noCarte);
	}
}

initialiseJeu(); // appel de la fonction initialisejeu

/* La fonction majAffichage met à jour l'affichage de la carte dont on passe le numéro en paramètre.
lorsque l'etat de la carte est a 0 on affiche l'image question mark(carte non retournee) et lorsque l'etat est a 1
la carte est retopurnee eet affiche l'image correspondant et enfin lorsque l'etat est de -1 la paire de carte a ete trouve et donc enleve*/

function majAffichage(noCarte) {
	switch (etatsCartes[noCarte]) {
		case 0:
			cards[noCarte].src = "./img/questionmark.png";
			break;
		case 1:
			cards[noCarte].src = "./img/img" + cartesImg[noCarte] + ".jpg";
			break;
		case -1:
			cards[noCarte].style.visibility = "hidden";
			break;
	}

}
//permet de rejouer la partie
function rejouer() {
	alert("Bravo!!!!!")
	location.reload();
}

//La fonction initialiseJeu mélange les numéros d'id des cartes.
function initialiseJeu() {
	for (var position = cartesImg.length - 1; position >= 1; position--) {
		var hasard = Math.floor(Math.random() * (position + 1));
		var sauve = cartesImg[position];
		cartesImg[position] = cartesImg[hasard];
		cartesImg[hasard] = sauve;
	}
}

//elle s'execute lorsquon clique sur la carte
function controleJeu(noCarte) {
	//Ici le nomre de carte retournee se limite a 2
	// si la premiere carte et la deuxieme carte n'ont pas le meme id, les deux cartes retourne a l'etat 0 alors elle execute la fonction majaffichage et met a jour l'affichage des cartes
	if (cartesRetournees.length < 2) {
		if (etatsCartes[noCarte] == 0) {
			etatsCartes[noCarte] = 1;
			cartesRetournees.push(noCarte);
			majAffichage(noCarte);
		}
		//si la premiere carte et la deuxieme carte ont le meme id alors elle sont a letat -1 et disparaissent 
		if (cartesRetournees.length == 2) {
			var nouveauEtat = 0;
			if (cartesImg[cartesRetournees[0]] == cartesImg[cartesRetournees[1]]) {
				nouveauEtat = -1;
				nbPairesTrouvees++;
				score += 10;
				document.getElementById('points').innerText = score
			} else {
				score -= 1;
				document.getElementById('points').innerText = score
			}


			etatsCartes[cartesRetournees[0]] = nouveauEtat;
			etatsCartes[cartesRetournees[1]] = nouveauEtat;

			//on implemente un temps d'affichge des carte retournee de 750ms
			setTimeout(function () {
				majAffichage(cartesRetournees[0]);
				majAffichage(cartesRetournees[1]);
				cartesRetournees = [];
				if (nbPairesTrouvees == 6) {
					jeu2(); // si toutes les paires sont trouvees il execute la fonction jeu2(deuxieme vague de cartes)
					niveau += 1;
					document.getElementById('niveau1').innerText = niveau;
				}
			}, 750);
		}
	}
}

//initialisation du jeu pour le niveau 2 avec plus de cartes et des triplets a trouver

function jeu2() {

	let cartesImg2 = [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7];
	let etatsCartes2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	let cartesRetournees2 = [];
	let nbPairesTrouvees2 = 0;
	cardshow.style.display = "none";
	carteshow.style.display = "flex";

	for (var j = 0; j < carte2.length; j++) {
		carte2[j].noCarte = j;
		carte2[j].onclick = function () {
			controleJeu(this.noCarte);
		}
	}

	initialiseJeu();

	function majAffichage(noCarte) {
		switch (etatsCartes2[noCarte]) {
			case 0:
				carte2[noCarte].src = "./img/questionmark.png";
				break;
			case 1:
				carte2[noCarte].src = "./img/img" + cartesImg2[noCarte] + ".jpg";
				break;
			case -1:
				carte2[noCarte].style.visibility = "hidden";
				break;
		}
	}

	function initialiseJeu() {
		for (var position = cartesImg2.length - 1; position >= 1; position--) {
			var hasard = Math.floor(Math.random() * (position + 1));
			var sauve = cartesImg2[position];
			cartesImg2[position] = cartesImg2[hasard];
			cartesImg2[hasard] = sauve;
		}
	}

	function controleJeu(noCarte) {
		if (cartesRetournees2.length < 3) {
			if (etatsCartes2[noCarte] == 0) {
				etatsCartes2[noCarte] = 1;
				cartesRetournees2.push(noCarte);
				majAffichage(noCarte);
			}
			if (cartesRetournees2.length == 3) {
				var nouveauEtat = 0;
				if (cartesImg2[cartesRetournees2[0]] == cartesImg2[cartesRetournees2[1]]) {

					if (cartesImg2[cartesRetournees2[1]] == cartesImg2[cartesRetournees2[2]]) {
						nouveauEtat = -1;
						nbPairesTrouvees2++;
						score += 20;
						document.getElementById('points').innerText = score
					}
				} else {
					score -= 5;
					document.getElementById('points').innerText = score
				}


				etatsCartes2[cartesRetournees2[0]] = nouveauEtat;
				etatsCartes2[cartesRetournees2[1]] = nouveauEtat;
				etatsCartes2[cartesRetournees2[2]] = nouveauEtat;
				setTimeout(function () {
					majAffichage(cartesRetournees2[0]);
					majAffichage(cartesRetournees2[1]);
					majAffichage(cartesRetournees2[2]);
					cartesRetournees2 = [];
					if (nbPairesTrouvees2 == 7) {
						niveau += 1;
						document.getElementById('niveau1').innerText = niveau;
						jeu3();
					}
				}, 750);
			}
		}
	}


}
//initialisation du jeu pour le niveau 3 avec plus de cartes et des quadruplets a trouver
function jeu3() {
	let cartesImg3 = [1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 10];
	let etatsCartes3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	let cartesRetournees3 = [];
	let nbPairesTrouvees3 = 0;
	carteshow.style.display = "none";
	carte2show.style.display = "flex";

	for (var k = 0; k < carte3.length; k++) {
		carte3[k].noCarte = k;
		carte3[k].onclick = function () {
			controleJeu(this.noCarte);
		}
	}

	initialiseJeu();

	function majAffichage(noCarte) {
		switch (etatsCartes3[noCarte]) {
			case 0:
				carte3[noCarte].src = "./img/questionmark.png";
				break;
			case 1:
				carte3[noCarte].src = "./img/img" + cartesImg3[noCarte] + ".jpg";
				break;
			case -1:
				carte3[noCarte].style.visibility = "hidden";
				break;
		}
	}

	function initialiseJeu() {
		for (var position = cartesImg3.length - 1; position >= 1; position--) {
			var hasard = Math.floor(Math.random() * (position + 1));
			var sauve = cartesImg3[position];
			cartesImg3[position] = cartesImg3[hasard];
			cartesImg3[hasard] = sauve;
		}
	}

	function controleJeu(noCarte) {
		if (cartesRetournees3.length < 4) {
			if (etatsCartes3[noCarte] == 0) {
				etatsCartes3[noCarte] = 1;
				cartesRetournees3.push(noCarte);
				majAffichage(noCarte);
			}
			if (cartesRetournees3.length == 4) {
				var nouveauEtat = 0;
				if (cartesImg3[cartesRetournees3[0]] == cartesImg3[cartesRetournees3[1]]) {

					if (cartesImg3[cartesRetournees3[1]] == cartesImg3[cartesRetournees3[2]]) {

						if (cartesImg3[cartesRetournees3[2]] == cartesImg3[cartesRetournees3[3]]) {

							nouveauEtat = -1;
							nbPairesTrouvees3++;
							score += 50;
							document.getElementById('points').innerText = score
						}
					}
				} else {
					score -= 10;
					document.getElementById('points').innerText = score
				}


				etatsCartes3[cartesRetournees3[0]] = nouveauEtat;
				etatsCartes3[cartesRetournees3[1]] = nouveauEtat;
				etatsCartes3[cartesRetournees3[2]] = nouveauEtat;
				etatsCartes3[cartesRetournees3[3]] = nouveauEtat;
				setTimeout(function () {
					majAffichage(cartesRetournees3[0]);
					majAffichage(cartesRetournees3[1]);
					majAffichage(cartesRetournees3[2]);
					majAffichage(cartesRetournees3[3]);
					cartesRetournees3 = [];

					// des que l'utilisateur atteint le niveau 3 et trouve toute les paires avant que le temps soit < 0
					//un popup gagner s'affiche 
					//si le nombe de paire nest pas trouver avant que le temps s'ecoule il s'affiche game over
					if (nbPairesTrouvees3 == 7) {

						rejouer();

					} 
				}, 750);
			}
		}
	}
}
//initialisation du compte a rebours
var echeance = new Date().getTime() + 10 * 60 * 1000;

var x = setInterval(function () {

	var heureCourante = new Date().getTime();
	var temps = echeance - heureCourante;
	var minutes = Math.floor((temps % (1000 * 60 * 60)) / (1000 * 60));
	var secondes = Math.floor((temps % (1000 * 60)) / 1000);

	document.getElementById("minute").innerHTML = minutes;
	document.getElementById("seconde").innerHTML = secondes;
	if (temps < 0) {
		clearInterval(x);
		document.getElementById("minute").innerHTML = '0';
		document.getElementById("seconde").innerHTML = '0';
		showPopup();

	}
}, 1000);

//la fonction qui permet de cacher le popup
let popupBox = document.getElementById('popup');

function hidePopup() {
	popupBox.style.display = "none";
}

//initialisation du pupup game over
function showPopup() {
	popupBox.style.display = "block";
	document.getElementById("restart-button").onclick = () => location.reload();
}